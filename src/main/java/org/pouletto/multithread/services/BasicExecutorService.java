package org.pouletto.multithread.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RequiredArgsConstructor
@Slf4j
public class BasicExecutorService {

  private final ThreadExample executor;
  ExecutorService threadPoolExecutor = Executors.newCachedThreadPool();

  public BigDecimal tryMultiplesComputations(List<BigDecimal> numbers, LocalDateTime date) {

    CompletableFuture<BigDecimal>[] futures =  numbers.stream()
      .map(nb ->  buggedOne(nb, date))
      .toArray(CompletableFuture[]::new)
      ;
    log.info("BEFORE ALL OF");
    AtomicReference<BigDecimal> total = new AtomicReference<>(BigDecimal.ZERO);

    CompletableFuture.allOf(futures).join();

    Arrays.stream(futures).forEach(data ->{
      try{
        var result = data.get();
        total.set(total.get().add(result));
        log.info("data {}", result);
      }catch (Exception e){
        log.error("During execution", e);
      }


    });

    return total.get();
  }

  public CompletableFuture<BigDecimal> buggedOne(BigDecimal number, LocalDateTime expiry) {
    CompletableFuture<BigDecimal> futur= new CompletableFuture<>();

    threadPoolExecutor.submit(() -> {
      try{
        futur.complete( executor.execute(number, expiry));
      }catch (Exception e){
        log.error("Will not show fdp", e);
      }

    });

    return futur;
  }

  public CompletableFuture<BigDecimal> prepareExecutionSol1(BigDecimal number, LocalDateTime expiry) {
    CompletableFuture<BigDecimal> futur= new CompletableFuture<>();

    threadPoolExecutor.submit(() -> {
      try{
        var res = executor.execute(number, expiry);
        futur.complete(res);
      }catch (Exception e){
        log.error("Catched fdp", e);
        futur.completeExceptionally(new RuntimeException("Catched fdp"));
      }

    });

    return futur;
  }


}
