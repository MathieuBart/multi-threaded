package org.pouletto.multithread.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Component
@Slf4j
public class ThreadExample {
  final int loopMax = 4;

  public BigDecimal execute(BigDecimal entry, LocalDateTime expiry)  {
    boolean isOk = false;

    int actual = 0;
    log.info("Starting calculation for {}", entry);
    do{
      try{
        if(simulateDelay(actual)){
          log.info("Retrieve done");
          break;
        }
      }catch (Exception e){
        log.info("Retrieve failed", e);
      }

      if(waitAndCheckExpired(expiry)){
        break;
      }
      actual++;
    }while(!isOk);

    if(entry.compareTo(new BigDecimal(0)) == 0){
      throw new RuntimeException("Boomas");
    }

    log.info("Before calculation");
    return entry.multiply(BigDecimal.valueOf(2));
  }

  private boolean simulateDelay(int actual)  {
    if(actual >= loopMax){
      return true;
    }

    try{
      Thread.sleep(2000);
    }catch (Exception exception){
      log.error("Simulate delay exception", exception);
    }

    return false;
  }

  private boolean waitAndCheckExpired(LocalDateTime expiry) {
    try{
      Thread.sleep(2000);
    }catch (Exception exception){
      log.error("waitAndCheckExpired", exception);
    };

    return expiry.isBefore(LocalDateTime.now());
  }

}
