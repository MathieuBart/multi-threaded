package org.pouletto.multithread.controllers;

import lombok.RequiredArgsConstructor;
import org.pouletto.multithread.services.BasicExecutorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@RestController("/basic")
@RequiredArgsConstructor
public class BasicController {
  private final BasicExecutorService executor;

  @PostMapping("/try")
  public ResponseEntity<BigDecimal> tryResult(@RequestBody List<BigDecimal> values){

    return ResponseEntity.ok(executor.tryMultiplesComputations(values, LocalDateTime.now().plusSeconds(10)));
  }


}
